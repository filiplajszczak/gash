#!@GUILE@ --no-auto-compile
-*- scheme -*-
!#

;;; The Geesh Shell Interpreter
;;; Copyright 2017 Timothy Sample <samplet@ngyro.com>
;;;
;;; This file is part of Geesh.
;;;
;;; Geesh is free software: you can redistribute it and/or modify
;;; it under the terms of the GNU General Public License as published by
;;; the Free Software Foundation, either version 3 of the License, or
;;; (at your option) any later version.
;;;
;;; Geesh is distributed in the hope that it will be useful,
;;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;;; GNU General Public License for more details.
;;;
;;; You should have received a copy of the GNU General Public License
;;; along with Geesh.  If not, see <http://www.gnu.org/licenses/>.

(add-to-load-path "@MODDIR@")
(set! %load-compiled-path (cons "@GODIR@" %load-compiled-path))

(use-modules (geesh repl)
             (geesh environment)
             (ice-9 getopt-long))

(define options-spec
  '((command (single-char #\c) (value #t))
    (errexit (single-char #\e))
    (stdin (single-char #\s))
    (xtrace (single-char #\x))))

(let* ((options (getopt-long (program-arguments) options-spec
                             #:stop-at-first-non-option #t))
       (command (option-ref options 'command #f))
       (errexit? (option-ref options 'errexit #f))
       (stdin (option-ref options 'stdin #f))
       (xtrace? (option-ref options 'xtrace #f))
       (args (option-ref options '() '())))
  (when errexit?
    (setopt! 'errexit #t))
  (when xtrace?
    (setopt! 'xtrace #t))
  (cond
   ((and command stdin)
    (format (current-error-port)
            "~a: Invalid options.~%"
            (car (program-arguments)))
    (exit EXIT_FAILURE))
   (command
    (if (null? args)
        (set-program-arguments (list (car (program-arguments))))
        (set-program-arguments args))
    (call-with-input-string command
      (lambda (port)
        (exit (run-repl port)))))
   (stdin
    (set-program-arguments (cons (car (program-arguments)) args))
    (exit (run-repl)))
   (else
    (cond
     ((null? args)
      (set-program-arguments (list (car (program-arguments))))
      (exit (run-repl)))
     (else
      (set-program-arguments args)
      (call-with-input-file (car args)
        (lambda (port)
          (exit (run-repl port)))))))))
